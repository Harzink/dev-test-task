const express = require('express')
const bodyParser = require('body-parser')
const db = require('./db')
const routes = require('./api/routes')
const config = require('./config')
const contentTypeSetter = require('./middleware/content-type-setter')


async function run () {
  const app = express()

  const dbConnectionSucces = await db.connect()
  if (!dbConnectionSucces) return false

  app.use(bodyParser.json({ type: 'application/json' }))
  app.use(contentTypeSetter)
  app.use('/api', routes)


  app.listen(config.port, () => {
    console.log(`App running on localhost:${config.port}`)
  });
}

module.exports = run
