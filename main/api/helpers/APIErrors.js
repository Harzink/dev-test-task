function processDbError (err)  {
  const errors = {}

  Object.keys(err.errors).forEach(k => errors[k] = err.errors[k].name)
  
  let status = err.name === 'ValidationError' ? 400 : 500

  const errorData = {
    message: err._message,
    errors,
    status
  }

  return errorData
}

module.exports = { processDbError }