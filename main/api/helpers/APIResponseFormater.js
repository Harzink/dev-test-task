module.exports = function  (data, res) {
  res = res || {}
  data = data || { status: 'success' }

  return {
    status: res.statusCode || 200,
    message: 'ok',
    data
  }
}