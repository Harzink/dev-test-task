const router = require('express').Router()
const messagesRoute = require('./modules/messages/route')


router.use('/messages', messagesRoute)

module.exports = router