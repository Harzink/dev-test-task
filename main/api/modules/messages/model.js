const mongoose = require('mongoose');


const MessageSchema = new mongoose.Schema({
  author: {
    type: String,
    maxlength: 50,
    match: /^[a-zA-Z0-9]*$/,
    required:true
  },
  text: {
    type: String,
    maxlength: 200,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

MessageSchema.statics = {
  
  list () {
    return this.find({}, { _id: 0, __v: 0 })
      .sort({ createdAt: -1 })
      .exec();
  }

}

module.exports = mongoose.model('Messages', MessageSchema)