const model = require('./model')
const { processDbError } = require('../../helpers/APIErrors')
const processResponse = require('../../helpers/APIResponseFormater')


async function list (req, res) {
  try {
    const messages = await model.list()
    res.status(200).json(processResponse(messages))
  }
  catch (err) {
    const errorData = processDbError(err)
    res.status(errorData.status).json(errorData)
  }
}

function create (req, res) {
  const message = {
    author: req.body.author,
    text: req.body.text
  }

  model.create(message, (err) => {
    if (err) {
      const errorData = processDbError(err)
      res.status(errorData.status).json(errorData)
    }
    else {
      res.status(200).json(processResponse())
    }
  });
}

module.exports = { list, create }