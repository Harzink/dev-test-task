const mongoose = require('mongoose');
const config = require('./config');

module.exports = async () => {
  try {
    await mongoose.connect(config.connectionString, { auth: config.auth })
    return true
  }
  catch (err) {
    console.log(err);
    return false
  }
}